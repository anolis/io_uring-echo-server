/*
 * Description: use io_uring nop tests to evaluate io_uring framework overhead.
 * xiaoguang.wang@linux.alibaba.com
 *
 */

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/syscall.h>

#include <liburing.h>

static char *myprog;
static int batch_count = 16;
static int force_async = 0;
static int runtime = 30;
static unsigned long long ios;
static volatile int stop = 0;
uint64_t total_time, count;
uint64_t base_overhead;

#define BILLION 1000000000L

#define TS_START() do { \
	struct timespec start, end; \
	uint64_t diff; \
	clock_gettime(CLOCK_MONOTONIC, &start);

#define TS_END(name, round) \
	clock_gettime(CLOCK_MONOTONIC, &end); \
	diff = (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec); \
	printf("%s : %.2lf ns\n", name, diff * 1.0 / round); \
} while (0)

typedef void (*test_fn)(void);

void getpid_test(void)
{
	syscall(SYS_getpid);
}

void getpid_libc_test(void)
{
    getpid();
}

void getpid_int80_test(void)
{
	unsigned syscall_nr = 20;
	unsigned exit_status = 99;

	__asm__ __volatile__ (
			      "movl %0, %%eax\n\t"  /* 调用号syscall_nr存入eax寄存器 */
			      "int $0x80\n\t"
			      "movl %%eax, %1\n\t"  /* 第一个参数返回码exit_status存入ebx寄存器 */
			      :
			      :"m"(syscall_nr) , "m"(exit_status)
			      :"eax", "ebx"
			     );
}

#define TEST(fn, des, warm_round, round) test(fn, des, warm_round, round)

void test(test_fn fn, const char *name, int warm_round, int round)
{
	int i;

	for (i = 0; i < warm_round; i++) {
		fn();
	}

	if (round <= 0)
		return;

	TS_START();
	for (i = 0; i < round; i++) {
		fn();
	}
	TS_END(name,round);
}

static void test_syscall_perf(void)
{
	TEST(getpid_test, "syscall user and kernel context switch overhead(use syscall(2))", 10, 5000000);
	TEST(getpid_int80_test, "syscall user and kernel context switch overhead(use int 80", 10, 5000000);

}

static void calc_base_time(void)
{
	struct timespec start, end;
	uint64_t diff, total = 0;
	int i;

	for (i = 0; i < 100000; i++) {
		clock_gettime(CLOCK_MONOTONIC, &start);
		clock_gettime(CLOCK_MONOTONIC, &end);
		diff = (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec);
		total += diff;
	}

	base_overhead = total / 100000;
}

static int test_nop(struct io_uring *ring)
{
	struct io_uring_cqe *cqe;
	struct io_uring_sqe *sqe;
	int i, ret;
	struct timespec start, end;
	uint64_t diff;

	for (i = 0; i < batch_count; i++) {
		sqe = io_uring_get_sqe(ring);
		if (!sqe) {
			fprintf(stderr, "get sqe failed\n");
			goto err;
		}
		io_uring_prep_nop(sqe);
		if (force_async)
			sqe->flags |= IOSQE_ASYNC;
	}

	clock_gettime(CLOCK_MONOTONIC, &start);
	ret = io_uring_submit(ring);
	if (ret <= 0) {
		fprintf(stderr, "sqe submit failed: %d\n", ret);
		goto err;
	}

	clock_gettime(CLOCK_MONOTONIC, &end);
	diff = (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec);
	total_time += diff;
	count++;


	for (i = 0; i < batch_count; i++) {
		ret = io_uring_wait_cqe(ring, &cqe);
		if (ret < 0) {
			fprintf(stderr, "wait completion %d\n", ret);
			goto err;
		}
		io_uring_cqe_seen(ring, cqe);
	}
	ios += batch_count;
	return 0;
err:
	return ret;
}

static void usage(void)
{
	printf("Usage: %s -H   or\n", myprog);
	printf("       %s [-b batch][-a][-r runtime]\n", myprog);
	printf("   -b  batch    submission batch count, default 16\n");
	printf("   -r  runtime  run time, default 30\n");
	printf("   -a           force asynchronous submission\n");
	printf("   -H           prints usage and exits\n");
}

static void alarm_handler(int signum)
{
	(void)signum;
	stop = 1;
}

int main(int argc, char *argv[])
{
	struct io_uring ring;
	struct sigaction sa;
	int ret, c;
	const char *opts = "b:ar:";

	myprog = argv[0];
	while ((c = getopt(argc, argv, opts)) != -1) {
		switch (c) {
		case 'b':
			batch_count = atoi(optarg);
			break;
                case 'a':
			force_async = 1;
			break;
                case 'r':
			runtime = atoi(optarg);
			break;
		case 'H':
			usage();
			exit(1);
		}
	}

	if (!batch_count) {
		fprintf(stderr, "batch count should be greater than 0\n");
		exit(1);
	}

	if (!runtime) {
		printf("run time is zero, are you sure?\n");
		return 0;
	}

	test_syscall_perf();
	calc_base_time();

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = alarm_handler;
	sigemptyset(&sa.sa_mask);
	ret = sigaction(SIGALRM, &sa, NULL);
	if (ret < 0) {
		fprintf(stderr, "sigaction failed: %s", strerror(errno));
		exit(1);
	}
	alarm(runtime);

	ret = io_uring_queue_init(batch_count, &ring, 0);
	if (ret) {
		fprintf(stderr, "ring setup failed: %d\n", ret);
		return 1;
	}

	while (!stop) {
		ret = test_nop(&ring);
		if (ret) {
			if (ret == -EINTR)
				break;
			fprintf(stderr, "test_nop failed\n");
			return ret;
		}
	}

	printf("io_uring batch count: %d\n", batch_count);
	printf("average lat per io_uring_submit: %lu\n", total_time / count - base_overhead);
	printf("average lat per nop op: %lu\n", (total_time / count - base_overhead) / batch_count);
	return 0;
}
