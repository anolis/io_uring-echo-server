CCFLAGS ?= -Wall -O2 -D_GNU_SOURCE -luring -lpthread
all_targets = io_uring_echo_server io_uring_echo_server_multi_shot epoll_echo_server io_uring_framework_eval io_uring_echo_server_v2

.PHONY: liburing io_uring_echo_server io_uring_echo_server_multi_shot epoll_echo_server io_uring_framework_eval io_uring_echo_server_v2

all: $(all_targets)

clean:
	rm -f $(all_targets)

liburing:
	+$(MAKE) -C ./liburing

io_uring_echo_server_multi_shot:
	$(CC) io_uring_echo_server_multi_shot.c -o ./io_uring_echo_server_multi_shot -I./liburing/src/include/ -L./liburing/src/  ${CCFLAGS}

io_uring_echo_server:
	$(CC) io_uring_echo_server.c -o ./io_uring_echo_server -I./liburing/src/include/ -L./liburing/src/  ${CCFLAGS}

io_uring_echo_server_v2:
	$(CC) io_uring_echo_server_v2.c -o ./io_uring_echo_server_v2 -luring  ${CCFLAGS}

io_uring_framework_eval:
	$(CC) io_uring_framework_eval.c -o ./io_uring_framework_eval -I./liburing/src/include/ -L./liburing/src/  ${CCFLAGS}

epoll_echo_server:
	$(CC) epoll_echo_server.c -o ./epoll_echo_server -Wall -O2 -D_GNU_SOURCE
